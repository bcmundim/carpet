# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Jun 24 2013 at 10:56:02-0400
# parameter filename: "../../arrangements/Carpet/CarpetReduce/test/periodic_weight.par"
#
# CARPETREDUCE::WEIGHT z (carpetreduce::weight)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:weight
0	0	0 0 0	13 13 0	0	0 0 -2.6	0
0	0	0 0 0	13 13 1	0	0 0 -2.4	0
0	0	0 0 0	13 13 2	0	0 0 -2.2	0
0	0	0 0 0	13 13 3	0	0 0 -2	1
0	0	0 0 0	13 13 4	0	0 0 -1.8	1
0	0	0 0 0	13 13 5	0	0 0 -1.6	1
0	0	0 0 0	13 13 6	0	0 0 -1.4	1
0	0	0 0 0	13 13 7	0	0 0 -1.2	1
0	0	0 0 0	13 13 8	0	0 0 -1	1
0	0	0 0 0	13 13 9	0	0 0 -0.8	1
0	0	0 0 0	13 13 10	0	0 0 -0.6	1
0	0	0 0 0	13 13 11	0	0 0 -0.4	1
0	0	0 0 0	13 13 12	0	0 0 -0.2	1
0	0	0 0 0	13 13 13	0	0 0 0	1
0	0	0 0 0	13 13 14	0	0 0 0.2	1
0	0	0 0 0	13 13 15	0	0 0 0.4	1
0	0	0 0 0	13 13 16	0	0 0 0.6	1
0	0	0 0 0	13 13 17	0	0 0 0.8	1
0	0	0 0 0	13 13 18	0	0 0 1	1
0	0	0 0 0	13 13 19	0	0 0 1.2	1
0	0	0 0 0	13 13 20	0	0 0 1.4	1
0	0	0 0 0	13 13 21	0	0 0 1.6	1
0	0	0 0 0	13 13 22	0	0 0 1.8	1
0	0	0 0 0	13 13 23	0	0 0 2	0
0	0	0 0 0	13 13 24	0	0 0 2.2	0
0	0	0 0 0	13 13 25	0	0 0 2.4	0


