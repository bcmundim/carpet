# 1D ASCII output created by CarpetIOASCII
# created on kop195.datura.admin by ianhin on Dec 10 2013 at 15:35:14+0100
# parameter filename: "/lustre/datura/ianhin/simulations/test_interp_1/output-0000/arrangements/CarpetExtra/CarpetProlongateTest/test/test_interp.par"
#
# CARPETPROLONGATETEST::INTERP_DIFFERENCE y (carpetprolongatetest::interp_difference)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:interp_x 14:interp_y 15:interp_z 16:interp_u 17:interp_u0 18:interp_du
0	0	0 0 0	0 0 0	0	0 0 0	-0.5 -0.5 -0.5 -127.001953125 -127.001953125 0
0	0	0 0 0	0 1 0	0	0 1 0	-0.5 -0.166666666666667 -0.5 -125.47779224537 -125.47779224537 1.4210854715202e-14
0	0	0 0 0	0 2 0	0	0 2 0	-0.5 0.166666666666667 -0.5 -122.087311921296 -122.087311921296 2.8421709430404e-14
0	0	0 0 0	0 3 0	0	0 3 0	-0.5 0.5 -0.5 -116.083984375 -116.083984375 0


# iteration 256   time 0.5
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:interp_x 14:interp_y 15:interp_z 16:interp_u 17:interp_u0 18:interp_du
256	0	0 0 0	0 0 0	0.5	0 0 0	-0.5 -0.5 -0.5 -127.250488288701 -127.250488288701 0
256	0	0 0 0	0 1 0	0.5	0 1 0	-0.5 -0.166666666666667 -0.5 -125.723344718141 -125.723344718141 1.4210854715202e-14
256	0	0 0 0	0 2 0	0.5	0 2 0	-0.5 0.166666666666667 -0.5 -122.326229428528 -122.326229428528 2.8421709430404e-14
256	0	0 0 0	0 3 0	0.5	0 3 0	-0.5 0.5 -0.5 -116.31115373224 -116.31115373224 0


# iteration 512   time 1
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:interp_x 14:interp_y 15:interp_z 16:interp_u 17:interp_u0 18:interp_du
512	0	0 0 0	0 0 0	1	0 0 0	-0.5 -0.5 -0.5 -381.005859375 -381.005859375 0
512	0	0 0 0	0 1 0	1	0 1 0	-0.5 -0.166666666666667 -0.5 -376.433376736111 -376.433376736111 -5.6843418860808e-14
512	0	0 0 0	0 2 0	1	0 2 0	-0.5 0.166666666666667 -0.5 -366.261935763889 -366.261935763889 5.6843418860808e-14
512	0	0 0 0	0 3 0	1	0 3 0	-0.5 0.5 -0.5 -348.251953125 -348.251953125 0


