# 1D ASCII output created by CarpetIOASCII
# created on nvidia by eschnetter on Jul 23 2015 at 21:50:37-0400
# parameter filename: "/xfs1/eschnetter/simulations/testsuite-nvidia-Cvanilla-sim-procs000002/output-0000/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# Xt1 d (Xt1)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0.018717388175794
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	0.0199595578514658
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	0.0207072636716955
0	0 0 0 0	3 3 3	0	-1 0 0	0.0209435273582111
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	0.0206641143816261
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	0.01987748298135
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	0.0186044217159981
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	0.0183327687028248
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	0.0196944292978192
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	0.0205678368340736
1	0 0 0 0	3 3 3	0.0125	-1 0 0	0.0209328573634514
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	0.0207820626186628
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	0.0201207597873076
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	0.0189667050163356
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	0.017919606544375
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	0.0193987246972944
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	0.0203965737970323
2	0 0 0 0	3 3 3	0.025	-1 0 0	0.020889886735545
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	0.0208680432993995
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	0.0203331816135943
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	0.0192999889801834
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	0.017478526212709
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	0.0190728812425485
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	0.0201937155207244
3	0 0 0 0	3 3 3	0.0375	-1 0 0	0.0208146565388131
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	0.0209218990445146
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	0.0205143989318894
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	0.0196037426704896
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	0.0170101970420742
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	0.018717383997537
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	0.0199595525707758
4	0 0 0 0	3 3 3	0.05	-1 0 0	0.0207072578707933
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	0.0209435216786136
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	0.0206641094100103
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	0.0198774792686559
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


