# 1D ASCII output created by CarpetIOASCII
# created on nvidia by eschnetter on Jul 23 2015 at 21:50:32-0400
# parameter filename: "/xfs1/eschnetter/simulations/testsuite-nvidia-Cvanilla-sim-procs000002/output-0000/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small-amr.par"
#
# gt33 d (gt33)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	1.00151444728458
0	0 0 0 0	2 2 2	0	-1.1 -0.1 -0.1	1.00103058733846
0	0 0 0 0	4 4 4	0	-1.05 -0.05 -0.05	1.00052158419456
0	0 0 0 0	6 6 6	0	-1 0 0	1
0	0 0 0 0	8 8 8	0	-0.95 0.05 0.05	0.999478687713693
0	0 0 0 0	10 10 10	0	-0.9 0.1 0.1	0.998970473678328
0	0 0 0 0	12 12 12	0	-0.85 0.15 0.15	0.998487842797788
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 1 0 0	3 3 3	0	-1.075 -0.075 -0.075	1.00077845405105
0	0 1 0 0	4 4 4	0	-1.05 -0.05 -0.05	1.00052158419456
0	0 1 0 0	5 5 5	0	-1.025 -0.025 -0.025	1.00026156452113
0	0 1 0 0	6 6 6	0	-1 0 0	1
0	0 1 0 0	7 7 7	0	-0.975 0.025 0.025	0.99973850387698
0	0 1 0 0	8 8 8	0	-0.95 0.05 0.05	0.999478687713693
0	0 1 0 0	9 9 9	0	-0.925 0.075 0.075	0.999222151468286
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 1 0 0	3 3 3	0.00625	-1.075 -0.075 -0.075	1.00084199286665
1	0 1 0 0	4 4 4	0.00625	-1.05 -0.05 -0.05	1.00058615932378
1	0 1 0 0	5 5 5	0.00625	-1.025 -0.025 -0.025	1.0003267771749
1	0 1 0 0	6 6 6	0.00625	-1 0 0	1.00006544788899
1	0 1 0 0	7 7 7	0.00625	-0.975 0.025 0.025	0.999803781991429
1	0 1 0 0	8 8 8	0.00625	-0.95 0.05 0.05	0.999543381011024
1	0 1 0 0	9 9 9	0.00625	-0.925 0.075 0.075	0.999286087066253
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	1.00163006455055
2	0 0 0 0	2 2 2	0.0125	-1.1 -0.1 -0.1	1.00115438931684
2	0 0 0 0	4 4 4	0.0125	-1.05 -0.05 -0.05	1.00065051253762
2	0 0 0 0	6 6 6	0.0125	-1 0 0	1.00013087500882
2	0 0 0 0	8 8 8	0.0125	-0.95 0.05 0.05	0.999608366131946
2	0 0 0 0	10 10 10	0.0125	-0.9 0.1 0.1	0.99908799614536
2	0 0 0 0	12 12 12	0.0125	-0.85 0.15 0.15	0.998605440679487
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 1 0 0	3 3 3	0.0125	-1.075 -0.075 -0.075	1.00090521095465
2	0 1 0 0	4 4 4	0.0125	-1.05 -0.05 -0.05	1.00065051253762
2	0 1 0 0	5 5 5	0.0125	-1.025 -0.025 -0.025	1.00039186829439
2	0 1 0 0	6 6 6	0.0125	-1 0 0	1.00013087500882
2	0 1 0 0	7 7 7	0.0125	-0.975 0.025 0.025	0.999869121993574
2	0 1 0 0	8 8 8	0.0125	-0.95 0.05 0.05	0.999608366131946
2	0 1 0 0	9 9 9	0.0125	-0.925 0.075 0.075	0.999350239030478
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 1 0 0	3 3 3	0.01875	-1.075 -0.075 -0.075	1.00096808386453
3	0 1 0 0	4 4 4	0.01875	-1.05 -0.05 -0.05	1.00071461895344
3	0 1 0 0	5 5 5	0.01875	-1.025 -0.025 -0.025	1.00045681377969
3	0 1 0 0	6 6 6	0.01875	-1 0 0	1.00019625064433
3	0 1 0 0	7 7 7	0.01875	-0.975 0.025 0.025	0.999934492267388
3	0 1 0 0	8 8 8	0.01875	-0.95 0.05 0.05	0.999673762798512
3	0 1 0 0	9 9 9	0.01875	-0.925 0.075 0.075	0.999413798113376
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	1.00174317959236
4	0 0 0 0	2 2 2	0.025	-1.1 -0.1 -0.1	1.00127642539051
4	0 0 0 0	4 4 4	0.025	-1.05 -0.05 -0.05	1.00077845406451
4	0 0 0 0	6 6 6	0.025	-1 0 0	1.00026153615454
4	0 0 0 0	8 8 8	0.025	-0.95 0.05 0.05	0.999739582917329
4	0 0 0 0	10 10 10	0.025	-0.9 0.1 0.1	0.999194674022561
4	0 0 0 0	12 12 12	0.025	-0.85 0.15 0.15	0.998724066065527
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 1 0 0	3 3 3	0.025	-1.075 -0.075 -0.075	1.00103058723084
4	0 1 0 0	4 4 4	0.025	-1.05 -0.05 -0.05	1.00077845406451
4	0 1 0 0	5 5 5	0.025	-1.025 -0.025 -0.025	1.00052158943903
4	0 1 0 0	6 6 6	0.025	-1 0 0	1.00026153615454
4	0 1 0 0	7 7 7	0.025	-0.975 0.025 0.025	0.999999905648928
4	0 1 0 0	8 8 8	0.025	-0.95 0.05 0.05	0.999739582917329
4	0 1 0 0	9 9 9	0.025	-0.925 0.075 0.075	0.999475833588443
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


